package course.examples.practica_04;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class Main3Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);

        Intent intent = getIntent();
        String message = intent.getStringExtra(MainActivity.EXTRA_MESSAGE);

        double lado = Double.parseDouble(message);
        lado = (lado*lado);

        TextView textView = new TextView(this);
        textView.setTextSize(40);
        textView.setText("Área: "+lado);

        setContentView(textView);
    }
}
